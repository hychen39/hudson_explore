package net.hudsonlifestyle;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertTrue;

public class HelloWorldTest {

    @Test
    public void  testMain() {
        ByteArrayOutputStream capture = new ByteArrayOutputStream();
        PrintStream newOut = new PrintStream(capture);
        System.setOut(newOut);

        HelloWorld.main(null);
        System.out.println(capture.toString());

        assertTrue(capture.toString().contains("Hello World"));
    }
}